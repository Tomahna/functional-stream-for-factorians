package fr.tomahna

sealed trait Item
case class Cable() extends Item
case class Circuit() extends Item
