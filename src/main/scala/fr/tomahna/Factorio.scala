package fr.tomahna

import cats.effect
import cats.effect.ExitCode
import cats.effect.IO
import cats.effect.IOApp
import cats.effect.unsafe.IORuntime
import cats.effect.unsafe.implicits.global
import fr.tomahna.Iron
import fr.tomahna.IronOre
import scribe.Logger
import scribe.format._
import scribe.info

import java.lang
import scala.concurrent.duration._

object Factorio extends App {
  configureLogging()

  //Iron Mining
  import fs2.Stream

  val mineIron: IO[IronOre] =
    for {
      _ <- IO.sleep(2.second)
      _ <- log("Produced 1 Iron ore")
    } yield IronOre()

  def ironMiningDrill: Stream[IO, IronOre] =
    Stream.eval(mineIron).repeat

  def run(stream: Stream[IO, _]): Unit =
    stream.take(10).compile.drain.unsafeRunSync()

  // run(ironMiningDrill)

  import fs2.Pipe

  def smeltIron(ore: IronOre): IO[Iron] =
    for {
      _ <- IO.sleep(3.2.second)
      _ <- log("Smelted 1 Iron ore in 1 Iron plate")
    } yield Iron()

  val ironFurnace: Pipe[IO, IronOre, Iron] =
    (input: Stream[IO, IronOre]) => input.evalMap(smeltIron)

  val ironFurnaceWithBuffer: Pipe[IO, IronOre, Iron] =
    (input: Stream[IO, IronOre]) =>
      input.prefetchN(50).evalMap(smeltIron)

  // run(ironMiningDrill.through(ironFurnaceWithBuffer))

  import cats.effect.std.Queue
  val ironBelt: IO[Queue[IO, IronOre]] =
    Queue.bounded[IO, IronOre](50)

  val ironPipeline: Stream[IO, Iron] = for {
    belt <- Stream.eval(ironBelt)
    drill = ironMiningDrill.evalMap(belt.offer)
    furnace1 = Stream
      .repeatEval(belt.take)
      .through(ironFurnaceWithBuffer)
    furnace2 = Stream
      .repeatEval(belt.take)
      .through(ironFurnaceWithBuffer)
    iron <- (furnace1 merge furnace2) concurrently drill
  } yield iron

  // run(ironPipeline)

  //Copper Mining
  val mineCopper: IO[CopperOre] =
    for {
      _ <- IO.sleep(2.seconds)
      _ <- log("Produced 1 Copper ore")
    } yield CopperOre()

  val copperMiningDrill: Stream[IO, CopperOre] =
    Stream.repeatEval(mineCopper)

  def smeltCopper(ore: CopperOre): IO[Copper] =
    for {
      _ <- IO.sleep(3.2.second)
      _ <- log("Smelted 1 Copper ore in 1 Copper plate")
    } yield Copper()

  val copperSmelter: fs2.Pipe[IO, CopperOre, Copper] =
    _.evalMap(smeltCopper)

  val copperPipeline: Stream[IO, Copper] = for {
    belt <- Stream.eval(
      Queue.bounded[IO, CopperOre](50)
    )
    drill = copperMiningDrill.evalMap(belt.offer)
    furnace1 = Stream
      .repeatEval(belt.take)
      .through(copperSmelter)
    furnace2 = Stream
      .repeatEval(belt.take)
      .through(copperSmelter)
    copper <- (furnace1 merge furnace2) concurrently drill
  } yield copper

  def assembleCable(
      copper: Copper
  ): IO[List[Cable]] = for {
    _ <- IO.sleep(0.5.second)
    _ <- log("Built 2 Cable from 1 Copper plate")
  } yield List(Cable(), Cable())

  val cableAssembler: Pipe[IO, Copper, Cable] =
    (input: Stream[IO, Copper]) =>
      for {
        copperCables <- input
          .prefetchN(50)
          .evalMap(assembleCable)
        copperCable <- Stream.emits(copperCables)
      } yield copperCable

  val cablePipeline: Stream[IO, Cable] =
    copperPipeline.through(cableAssembler)

  import fs2.Chunk

  def assembleCircuit(
      cables: Chunk[Cable],
      plate: Iron
  ): IO[Circuit] =
    for {
      _ <- IO(assert(cables.size == 3))
      _ <- IO.sleep(0.5.second)
      _ <- log("Built 1 Circuit w/ 3 plates + 1 cable")
    } yield Circuit()

  val circuitAssembler
      : Pipe[IO, (Chunk[Cable], Iron), Circuit] =
    (input: Stream[IO, (Chunk[Cable], Iron)]) =>
      input
        .prefetchN(50)
        .evalMap { case (cables, plate) =>
          assembleCircuit(cables, plate)
        }

  val circuitPipeline: Stream[IO, Circuit] =
    (cablePipeline.chunkN(3, false) parZip ironPipeline)
      .through(circuitAssembler)

  // run(circuitPipeline)

  def splitterFanIn(
      left: Stream[IO, Iron],
      right: Stream[IO, Copper]
  ): Stream[IO, Metal] =
    (left merge right)

  def splitterFilter(
      input: Stream[IO, Metal]
  ): Stream[IO, (Stream[IO, Iron], Stream[IO, Copper])] =
    for {
      left <- Stream.eval(Queue.bounded[IO, Iron](0))
      leftOutput = Stream.repeatEval(left.take)

      right <- Stream.eval(Queue.bounded[IO, Copper](0))
      rightOutput = Stream.repeatEval(right.take)

      filter = input
        .evalTap(ore => log(s"Processing element $ore"))
        .evalMap {
          case copper: Copper => right.offer(copper)
          case iron: Iron     => left.offer(iron)
        }

      result <- Stream.emit(
        (leftOutput, rightOutput)
      ) concurrently filter
    } yield result

  run(
    splitterFilter(copperPipeline merge ironPipeline)
      .flatMap { case (ironStream, copperStream) =>
        copperStream.take(2) merge ironStream
      }
  )
}
