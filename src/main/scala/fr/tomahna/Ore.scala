package fr.tomahna

sealed trait Ore
case class IronOre() extends Ore
case class CopperOre() extends Ore
