package fr.tomahna

sealed trait Metal extends Product with Serializable
case class Iron() extends Metal
case class Copper() extends Metal
