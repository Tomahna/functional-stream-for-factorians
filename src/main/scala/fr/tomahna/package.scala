package fr

import cats.effect.IO
import perfolation._
import scribe.LogRecord
import scribe.Logger
import scribe.format._
import scribe.output.LogOutput
import scribe.output.TextOutput

package object tomahna {
  private val initialTimestamp = System.currentTimeMillis()

  object ElapsedTime extends FormatBlock {
    override def format[M](
        record: LogRecord[M]
    ): LogOutput = {
      val elapsedTimeStamp =
        record.timeStamp - initialTimestamp
      new TextOutput(
        s"${elapsedTimeStamp.t.M}:${elapsedTimeStamp.t.S}"
      )
    }
  }

  private val myFormatter: Formatter = Formatter.fromBlocks(
    ElapsedTime,
    space,
    green(methodName),
    string(" - "),
    message,
    mdc
  )

  def configureLogging() =
    Logger.root
      .clearHandlers()
      .withHandler(formatter = myFormatter)
      .replace()

  def log(message: => String)(implicit
      pkg: sourcecode.Pkg,
      fileName: sourcecode.FileName,
      name: sourcecode.Name,
      line: sourcecode.Line
  ) =
    IO(scribe.info(message))

}
