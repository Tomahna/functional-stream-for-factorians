import sbt._

object Dependencies {
  lazy val fs2 = "co.fs2" %% "fs2-io" % "3.1.1"
  lazy val scribe = "com.outr" %% "scribe" % "3.6.1"
}
